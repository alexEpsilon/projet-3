@extends('layout.layout')
@section('content')

<!--Navbar-->
<nav id="navbar" class="navbar navbar-expand-lg ">

  <!-- Navbar brand -->
  <img src="images/sources-homepage/logo/logo-white.png" class="logo">

  <!-- Collapse button -->

  <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"><i
        class="fa fa-bars fa-1x"></i></span></button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav">
      
      <li class="nav-item">
        <a class="nav-link " href="#section-3">Conception</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#section-4">Motorisation</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#section-5">Technologie</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#section-6">Intérieur</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#section-7">Caractéristiques</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#section-8">Configurateur</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#section-9">Galerie Photos</a>
      </li>

    </ul>
  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->

{{-- section 1 --}}
<div id="section-1" class="container-fluid section-1 section-height"></div>
{{-- end section 1 --}}

{{-- bandeau --}}
<div class="container-fluid">
    <div class="row">
        <div class="bandeau-droite float-left"></div><div class="bandeau-centre"></div><div class="bandeau-gauche float-right"></div>
    </div>
</div>
{{-- end bandeau --}}

{{-- section 2 --}}
<div id="section-2" class="container-fluid section-2 "></div>
{{-- end section 2 --}}

{{-- section 3 --}}
<div id="section-3" class="container-fluid section-3 zindexed2">
    <div class="row ">
        <h2 class="col-md-8 col-12 text-right font-weight-bold font-italic font-alpine">Conception</h2>
        <div class="">
            <div class="slash"></div>       
        </div>
        <p class="col-12 font-alpine-light text-center">Pour une voiture sportive, le poids est un élément essentiel. La répartition des masses optimale de l’A110, ses dimensions compactes et la légèreté de sa carrosserie en aluminium lui confèrent plaisir de conduite et agilité.</p>
        <div id="conception-paralax" class="col-12"> </div>
        
    </div>
    
    <div class="row margin-top">
         
        <h2 class="margin-top col-md-8 col-12 text-right font-alpine">Robustesse et légèreté</h3>
        <div class="margin-top col-4">
            <div class="slash"></div>
        </div>
        <div class="col-md-6 col-12">
            <p class="font-alpine-light text-center">L’A110 tire de sa structure en aluminium robustesse, extrême agilité et maniabilité. Le design épuré et les éléments de carrosserie rivetés et soudés font de nouveau gagner en légèreté et en robustesse.</p>
            <img src="images/sources-homepage/A110/siege.png" class="siege img-fluid">
        </div>
        <div class="col-md-6 col-12 ">
            <img src="images/sources-homepage/conception/conception_desktop.png" class="img-fluid zindexed">
        </div>
    </div>

</div>

{{-- end section 3 --}}

<div class="container-fluid">
    <div class="fond-bleu z-depth-3"></div>
</div>

<div id="section-4" class="container section-4">
    

    <div class="row">
        <h2 class="col-md-8 col-12 text-right font-italic font-weight-bold font-alpine font-white">Motorisation</h2>
       
        <p id="section-4-text" class="col-12 font-alpine-light text-center ">La légèreté de la structure associée à un moteur turbocompressé font de l’A110 un modèle ultra performant doté d’un rapport poids/puissance de 4,29 kg/ch. La preuve en est, l’A110 peut passer de 0 à 100 km/h en seulement 4,5 secondes.</p>
    </div>
    <div class="row margin-top">
        <div class="col-12 col-md-6">
            <img src="images/sources-homepage/motorisation/turbo_desktop.png" class="col-11">
        </div>
        <div class="col-12 col-md-6 row">
            <h3 class="font-alpine">turbo compressé</h3>
        <p class="text-center font-alpine-light">Assuré par une boîte à double embrayage, le système de transmission garantit des passages de vitesse fluides et rapides. Les palettes en aluminium permettent au conducteur un contrôle parfait du véhicule.</p>
        </div>
        <div class="row margin-top">
            <div class="col-12 col-md-6 row">
                <h3 class="col-md-8 col-12 font-alpine text-right">Moteur</h3>
                <div class="col-md-4">
                    <div class="slash"></div>
                </div>
                <p class="font-alpine-light text-center">Placé en position centrale arrière, le moteur turbocompressé 4 cylindres fait battre le cœur de l’A110. Réglé par les ingénieurs Alpine pour offrir une meilleure réponse à l'accélération, le moteur à injection directe 1,8 L. joue la carte de la performance. La signature sonore sportive ajoute quant à elle une part d’émotion.</p>
            </div>
            <video class="col-12 col-md-6"
                src="images/sources-homepage/motorisation/MOTEUR_CINEMAGRAPH-.mov"
                autoplay muted playsinline loop>
            </video>
        </div>
    </div>

    
</div>

<div id="section-5" class=" margin-top container section-5">
   <div class="row">
    <h2 class="col-md-8 col-12 text-right font-italic font-alpine text-section-5">Technologie</h2>
    <div class="col-4">
        <div class="slash"></div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">
            <img src="images/sources-homepage/technologie/suspension_desktop.png" class="col-12">
        </div>
        <div class="col-12 col-md-6 font-alpine-light text-center zindexed-3">
            <p>La nouvelle A110 couple les singularités propres à Alpine aux dernières technologies. La boîte de vitesse automatique et le choix entre trois modes de conduite permettent une meilleure appropriation du véhicule.</p>
            <img src="images/sources-homepage/versions/COMPO-PURE.png" class="img-fluid">
        </div>
    </div>
   </div>
</div>

<div class="container-fluid">
    <div class="fond-bleu-3 z-depth-3 zindexed-1"></div>
</div>

<div id="section-6" class="margin-top container-fluid section-6">
    <div class="row">
        <h2 class="col-md-8 col-12 text-right font-italic font-alpine">Intérieur</h2>
        <div class="col-4">
            <div class="slash"></div>
        </div>    
        <div class="col-12 col-md-6">
            <img class="img-fluid" src="images/sources-homepage/interieur/interieur_desktop.png">
        </div>
        <div class="col-12 col-md-6">
            <p class="font-alpine-light">A l’intérieur, l’émotion vient du contraste entre les matières chaudes et les matières froides. Les éléments en aluminium apparents se marient avec du cuir. Le carbone complète l’ambiance sportive. La présence d’une console centrale surélevée et de sièges baquets légers vont de paire avec l’esprit de légèreté de la structure. La climatisation et la connexion au smartphone garantissent eux liberté et confort.</p>
            <h2 class=" font-alpine">Les sièges</h2>
            <p class=" font-alpine-light">Avec seulement 13,1 kg chacun, les sièges baquet offrent un confort maximal en piste et participent de l’extrême légèreté de l’A110. Des sièges six-voies réglables, toujours très légers, sont également proposés en option.</p>
        </div>
    </div>
</div>

<div id="section-7" class="margin-top container-fluid section-7">
    <div class="row">
        <h2 class="col-md-8 col-12 text-center font-italic font-alpine">Caractéristique technique</h2>
        <div class="col-4">
            <div class="slash"></div>
        </div>
        <div class="offset-1 col-md-5 col-12">
            <img class="img-fluid" src="images/sources-homepage/caracteristiques/dimensions-tech.png">
        </div>
        <div class="tableau col-md-5 col-12">
            <ul>
                <li>
                    <p class="titre-tableau">Puissance moteur<span class="desc-tableau offset-5">252 ch (185kw)</span></p>
                </li>
                <hr><li>
                    <p class="titre-tableau">Accélération de 0 à 100 km/h<span class="desc-tableau offset-3">4,5 secondes</span></p>
                </li>
                <hr><li>
                    <p class="titre-tableau">Vitesse maximale (limitée éléctroniquement)<span class="desc-tableau offset-1">250 km/h</span></p>
                </li>
                <hr><li>
                    <p class="titre-tableau">Consommation en cycle mixte*<span class="desc-tableau offset-4">6,4 l/100</span></p>
                </li>
                <hr><li>
                    <p class="titre-tableau">Émissions CO2 en cycle mixte*<span class="desc-tableau offset-4">144 g/km</span></p>
                </li>
                <hr>
                <li>
                    <p class="titre-tableau">Boîte de vitesse<span class="desc-tableau offset-1">Automatique double embrayage à 7 rapports</span></p>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="section-8" class="margin-top container-fluid section-8 ">
    <h2 class="text-center font-italic font-alpine">Configurateur</h2>
    <div class="fond-bleu2 z-depth-3"></div>
    <div class="row">
        <div id="carouselExampleSlidesOnly" class="container-fluid carousel-first slide" data-ride="carousel">
            <div class="col-12 container carousel-inner">
                <div class="col-12 carousel-item active">
                    <img class="img-fluid offset-1 d-block w-60" src="images/sources-homepage/versions/ALPINE-PE.png">
                    </div>
                    <div class="carousel-item">
                    <img class="img-fluid offset-1 d-block w-60" src="images/sources-homepage/versions/ALPINE-PURE-1.png">
                    </div>
                    <div class="carousel-item">
                    <img class="img-fluid offset-1 d-block w-60" src="images/sources-homepage/versions/ALPINE-LEGENDE-1.png">
                </div>
            </div>
        </div>
    </div>
    <div class=" offset-5 col-2">
        <a role="button" class="btn btn-indigo btn-lg" href="{{ url('/configurateur') }}">Configurer mon model</a>
    </div>
</div>

{{-- section 9 --}}
<div id="section-9" class="container-fluid section-9 ">
        <div class="fond-bleu-galerie"></div>
        <div class="row background-blue">
            <div class="margin-left col-12 col-md-5">
                    <img src="images/sources-homepage/galerie/A110_PE_1.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal1">
                    <img src="images/sources-homepage/galerie/A110_LEGENDE_1.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal2"> 
            </div>
            
            <div class="col-12 col-md-6">
                    <img src="images/sources-homepage/galerie/A110_LEGENDE_5.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal3">
                    <img src="images/sources-homepage/galerie/A110_PE_9.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal4">
            </div>
        </div>
        <div class="fond-bleu-galerie2"></div>

        <div class="row">
            <div class="margin-left col-12 col-md-6">
                    <img src="images/sources-homepage/galerie/A110_PE_7.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal5">
                    <img src="images/sources-homepage/galerie/A110_PURE_8.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal6">
            </div>
            <div class="col-12 col-md-5">
                    <img src="images/sources-homepage/galerie/A110_PURE_4.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal7">
                    <img src="images/sources-homepage/galerie/A110_PURE_6.jpg" class="img-fluid z-depth-1" data-toggle="modal" data-target="#modal8"> 
            </div>
        </div>
    
</div>
{{-- end section 9 --}}

@include('modal')

@endsection


@section('scripts')
<script>
    $('.carousel-first').carousel({
  interval: 3000
})
</script>

<script>
(function($) {
    "use strict";

    var $navbar = $("#navbar"),
        y_pos = $navbar.offset().top,
        height = $navbar.height();

    $(document).scroll(function() {
        var scrollTop = $(this).scrollTop();

        if (scrollTop > (y_pos + height) ) {
            $(".logo").attr("src","images/sources-homepage/logo/logo.png")
            $navbar.addClass("navbar-fixed").animate({
                top: 0
            });
        } else if (scrollTop <= y_pos) {
            $(".logo").attr("src","images/sources-homepage/logo/logo-white.png")

            $navbar.removeClass("navbar-fixed").clearQueue().animate({
                top: 0
            }, 0);
        }
    });
})(jQuery, undefined);
</script>

@endsection