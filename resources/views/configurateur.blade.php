@extends('layout.layout')

@section('content')
<div class="header z-depth-1">
    <a role="button" href="{{ url('/') }}"><img src="images/sources-homepage/logo/Logoalpine2017.png" class="img-fluid logo2 center"></a>
</div>
<div id="app">
    {{-- carousel start --}}
    <div  class="container-fluid">
        @include('partials/carousel')
    </div>
    {{-- carousel end  --}}
        <hr>
    {{-- seat start --}}
    <div class="container">
        @include('partials/sellerie')
    </div>
    {{-- seat end  --}}
                <hr>
    {{-- equipements start --}}
    <div class="container equipement">
        @include('partials/equipement')
    </div>
    {{-- equipements end  --}}
                <hr>
    {{-- accessory start  --}}
    <div class="container accessoires">
        @include('partials/accessoire')
    </div>
  
    {{-- accessory end  --}}
    <div class="container-fluid">
        <div class="footer row">
            <div class="options col-md-7 col-12 ">
                <h3 class="font-white  font-weight-bold">Options: @{{detailsLength}}</h3>
                <span class="badge option-details" v-for="detail in details">@{{detail}}</span>         
            </div>
            <div class="col-md-3 col-12">
                <h4 class="font-white font-weight-bold">Model: @{{version}}</h4>
            </div>
            <div class="panier col-12 col-md-2">
                <h3 class="font-white text-center margin-auto font-weight-bold">Total : <span v-html="totalPrice"> </span></h3>
            </div>            
        </div>                
    </div>
{{-- div app vue.js --}}
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
var app = new Vue({
    el: '#app',
    data: {
        version : "pure",
        serie : [], 
        details : [],
        allselected : document.querySelectorAll(".selected"),
        detailsLength : 0,
        carIndex : 0,
        imagesCar : [  
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (4).jpg",
        ],
        imageJante : [  
                "images/configurateur/jantes/vues/couleur-blanc_jante-standard (2).jpg",
                "images/configurateur/jantes/vues/couleur-blanc_jante-serac (2).jpg",
                "images/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg",
        ],
        imageCarSet: [
            {   imageStandard : [
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (4).jpg",
                ],
                imageSerac : [
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac (4).jpg",
                ],
                imageLegende : [
                "images/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-4.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-1.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-3.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-2.jpg",
                ],
                imageJante : [
                "images/configurateur/jantes/vues/couleur-blanc_jante-standard (2).jpg",
                "images/configurateur/jantes/vues/couleur-blanc_jante-serac (2).jpg",
                "images/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg",
                ],
            },
            {  imageStandard : [
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (4).jpg",
                ],
                imageSerac : [
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (4).jpg",
                ],
                imageLegende : [
                "images/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-4.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-1.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-3.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-2.jpg",
                ],
                imageJante : [
                "images/configurateur/jantes/vues/couleur-bleu_jante-standard (3).jpg",
                "images/configurateur/jantes/vues/couleur-bleu_jante-serac (3).jpg",
                "images/configurateur/jantes/vues/couleur-bleu_jante-legende (3).jpg",
                ],
            },
             {  imageStandard : [
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (4).jpg",
                ],
                imageSerac : [
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (1).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (2).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (3).jpg",
                "images/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (4).jpg",
                ],
                imageLegende : [
                "images/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-4.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-1.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-3.jpg",
                "images/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-2.jpg",
                ],
                imageJante : [
                "images/configurateur/jantes/vues/couleur-noir_jante-standard (1).jpg",
                "images/configurateur/jantes/vues/couleur-noir_jante-serac (1).jpg",
                "images/configurateur/jantes/vues/couleur-noir_jante-legende (1).jpg",
                ],
            },
        ],
    },
    computed: {
        totalPrice : function(){
            let sum = 0;
            for (var i=0; i<this.allselected.length; i++){
                sum += parseInt(this.allselected[i].dataset.type);
            }
            return sum
        },
    },
    methods: {
        colorSelect: function(i, event, unique){
            this.carIndex = i;
            this.imagesCar = this.imageCarSet[this.carIndex].imageStandard;
            this.imageJante = this.imageCarSet[this.carIndex].imageJante;
            this.selectUnique(unique, event);
        },
        selectStandard : function(unique, event){
            this.imagesCar = this.imageCarSet[this.carIndex].imageStandard;
            this.selectUnique(unique, event);
        },
        selectSerac : function(unique, event){
            this.imagesCar = this.imageCarSet[this.carIndex].imageSerac;
            this.selectUnique(unique, event);
        },
        selectLegende : function(unique, event){
            this.imagesCar = this.imageCarSet[this.carIndex].imageLegende;
            this.selectUnique(unique, event);
        },
        selected : function(event){
            if(!event.target.className.includes("selected")){
                event.target.classList.add("selected");
                this.details.push(event.target.dataset.name);
                this.detailsLength = this.details.length;
                this.recapitulatif();

            } else if(event.target.className.includes("selected")) {
                event.target.classList.remove("selected"); 
                this.details = this.details.filter(e => e !== event.target.dataset.name);
                this.detailsLength = this.details.length; 
                this.recapitulatif();
            }            
        },
        selectPure : function(unique, event){
            if(this.version === 'legende'){
                this.version = 'pure';
                this.selectUnique(unique, event);
                this.recapitulatif();

            }
            this.version = 'pure';
        },
        selectLegende : function(unique, event){
            if(this.version === 'pure'){
                this.version = 'legende';
                this.selectUnique(unique, event);
                this.recapitulatif();

            }
            this.version = 'legende';
        },

        selectUnique : function(unique, event){
            compareUnique = document.querySelectorAll(unique);
            if(!event.target.className.includes("selected")){
                for (var i=0; i<compareUnique.length; i++){
                    if(compareUnique[i].classList.contains("selected")){
                        compareUnique[i].classList.remove('selected'); 
                        this.details = this.details.filter(e => e !== compareUnique[i].dataset.name);
                    }
                }
                event.target.classList.add("selected");
                this.details.push(event.target.dataset.name);
                this.detailsLength = this.details.length;                                                 
            } 
        },

        recapitulatif : function(){
            this.allselected = document.querySelectorAll(".selected");
        }
    },
})
</script>
@endsection