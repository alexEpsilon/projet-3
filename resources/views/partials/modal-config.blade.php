<!-- Modal -->
<div class="modal fade" id="modal-recap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-alpine offset-4" id="exampleModalLabel">Votre configuration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="font-alpine-air">• Model choisis : @{{version}}</p>
      <div class="container-fluid">
      <div class="row">
        <div v-for="selected in allselected" class="col-3">
          <img v-if="selected.dataset.button" :src="imagesCar[0]" alt="truc" class="img-fluid z-depth-1">
          <img v-if="!selected.dataset.button" :src="selected.src" alt="Model" class="img-fluid z-depth-1">
          <p class="option-details font-alpine-air badge badge-primary">@{{selected.dataset.name}} </p>
          <p v-if="selected.dataset.inclus" class="option-details font-alpine-air badge badge-primary badge-inclus">@{{selected.dataset.inclus}} </p>
          <p v-if="selected.dataset.type > 0" class="option-details font-alpine-air badge badge-primary badge-prix">@{{selected.dataset.type}} € </p>
        </div>
      </div>
        
      </div>
      </div>
      <div class="modal-footer">
        <h3 class="font-alpine float-left font-weight-bold">Total : <span v-html="totalPrice"></span></h3>
        <button type="button" class="btn blue-gradient btn-lg" data-dismiss="modal">Commander</button>
      </div>
    </div>
  </div>
</div>