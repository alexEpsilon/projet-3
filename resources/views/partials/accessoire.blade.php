<h3 class="font-alpine font-italic text-center">Accessoires</h3>
<div class="transport">
<div class="col-12 col-md-6 row">
    <h5 class="font-italic font-alpine">Transport<br>et protection</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="accessoire row">
    <div class="extincteurs col-6 col-md-3">                         
        <img src="images/configurateur/accessoires/transport_et_protection/extincteur.jpg" data-name="Extincteur 1kg avec manomètre" data-type="22" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">extincteur 1kg avec manomètre<br>22€</span></h6>  
    </div>  
    <div class="fix-extincteur col-6 col-md-3">              
        <img src="images/configurateur/accessoires/transport_et_protection/fixation-extincteur.jpg" data-name="Fixation extincteur" data-type="72" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Fixation extincteur<br>72€</span></h6>
    </div>
    <div class="kit-secure col-6 col-md-3">   
        <img src="images/configurateur/accessoires/transport_et_protection/kit-securite.jpg" data-name="Kit de sécurité" data-type="20" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Kit de sécurité<br>20€</span></h6>
    </div>
    <div class="chaine-neige col-6 col-md-3">      
        <img src="images/configurateur/accessoires/transport_et_protection/chaaine-neige.jpg" data-name="Chaine à neige premium" data-type="270" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Chaine à neige premium<br>270€</span></h6>
    </div>
    <div class="alarme col-6 col-md-3">             
        <img src="images/configurateur/accessoires/transport_et_protection/alarme.jpg" data-name="Alarme" data-type="543" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Alarme<br>543€</span></h6>
    </div>
    <div class="protection col-6 col-md-3">        
        <img src="images/configurateur/accessoires/transport_et_protection/protection-obd.jpg" data-name="Protection brise OBD" data-type="99" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Protection brise OBD<br>99€</span></h6>
    </div>                  
</div>
<div class="transport">
<div class="col-12 col-md-6 row">
    <h5 class="font-italic font-alpine">Multimedia</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="accessoire row">
    <div class="supp-camera col-6 col-md-3">
        <img src="images/configurateur/accessoires/multimedia/support-camera.jpg" data-name="Support caméra sport" data-type="89" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Support caméra sport<br>89€</span></h6>
    </div>
    <div class="supp-phone col-6 col-md-3">
        <img src="images/configurateur/accessoires/multimedia/support-smartphone.jpg" data-name="Support smartphone magnétique" data-type="21" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Support smartphone magnétique<br>21€</span></h6>
    </div>
</div>
<div class="transport">
<div class="col-12 col-md-6 row">
    <h5 class="font-italic font-alpine">Interieur</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="accessoire row">
    <div class="filet-range col-6 col-md-3">
        <img src="images/configurateur/accessoires/interieur/filet-rangement.jpg" data-name="Filet de rangement horizontal" data-type="59" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Filet de rangement horizontal<br>59€</span></h6>
    </div>
    <div class="tapis-coffre col-6 col-md-3">
        <img src="images/configurateur/accessoires/interieur/tapis-coffre.jpg" data-name="Tapis de coffre" data-type="83" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Tapis de coffre<br>83€</span></h6>
    </div>
</div>
<div class="transport">
    <div class="col-12 col-md-6 row">
        <h5 class="font-italic font-alpine">Exterieur</h5>
        <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="accessoire row">
    <div class="antivol col-6 col-md-3">
        <img src="images/configurateur/accessoires/exterieur/antivol-jantes.jpg" data-name="Antivol jantes - noirs" data-type="51" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Antivol jantes - noirs<br>51€</span></h6>
    </div>
    <div class="cabochon col-6 col-md-3">
        <img src="images/configurateur/accessoires/exterieur/cabochons-metal.jpg" data-name="Cabochons alpine métalisé" data-type="24" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Cabochons alpine métalisé<br>24€</span></h6>
    </div>
    <div class="housse col-6 col-md-3">
        <img src="images/configurateur/accessoires/exterieur/housse.jpg" data-name="Housse de protection alpine" data-type="216" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Housse de protection alpine<br>216€</span></h6>
    </div>
</div>
<div class="transport">
    <div class="col-12 col-md-6 row">
        <h5 class="font-italic font-alpine">Garage</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="row" >
    <div class="chargeur-batterie col-6 col-md-3">
        <img src="images/configurateur/accessoires/garage/chargeur-batterie.jpg" data-name="Chargeur batterie" data-type="240" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Chargeur batterie<br>240€</span></h6>
    </div>
    <div class="kit-outils col-6 col-md-3">
            <img src="images/configurateur/accessoires/garage/kit-outils.jpg"
            class="img-fluid z-depth-1" data-name="Kit outils alpine" data-type="398" v-on:click="selected">
        <h6><span class="badge badge-primary">Kit outils alpine<br>398€</span></h6>
    </div>
</div>

@include('partials/modal-config')

<div class="float-right btn-recap" id="accessoire">
    <button class="btn blue-gradient btn-lg" data-toggle="modal" v-on:click="recapitulatif" data-target="#modal-recap">Recapitulatif</button>
</div>
