<div class="row">
    <div id="carouselExampleControls" class="carousel slide col-12 col-md-7" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block w-100" :src="imagesCar[0]" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" :src="imagesCar[1]" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" :src="imagesCar[2]" alt="Third slide">
            </div>
                <div class="carousel-item">
                <img class="d-block w-100" :src="imagesCar[3]" alt="Four slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="col-md-5 col-12">
        <button type="button" v-on:click="selectPure('button.version', $event)" data-button="version" data-name="A110 Pure" data-type="54700" class="version choice-car btn blue-gradient selected">Pure</button>
        <button type="button" v-on:click="selectLegende('button.version', $event)" data-button="version" data-name="A110 Legende" data-type="58500" class="version choice-car btn blue-gradient">Legende</button>

        <h3 class="font-alpine font-italic">Couleurs</h3> 
        <hr>
        <div class=" choix-couleur row">
            <div class="blanc col-4">
                <img src="images/configurateur/couleurs/selection/blanc.jpg" data-type="0" data-name="Peinture opaque blanc glacier" class="img-fluid couleur selected" v-on:click="colorSelect(0, $event, 'img.couleur')">
                <h6><span class="desc-peinture badge badge-primary ">Peinture opaque<br>blanc glacier 0€</span></h6>
            </div>
            <div class="bleu col-4">
                <img src="images/configurateur/couleurs/selection/bleu.jpg" data-type="1800" data-name="Teinture spécial bleu alpine" class="img-fluid couleur" v-on:click="colorSelect(1, $event, 'img.couleur')" >
                <h6><span class="desc-peinture badge badge-primary">Teinte spécial<br> bleu alpine 1800€</span></h6>
            </div>
            <div class="noir col-4">
                <img src="images/configurateur/couleurs/selection/noir.jpg" data-type="840" data-name="Teinture métalisé noir profond" class="img-fluid couleur" v-on:click="colorSelect(2, $event, 'img.couleur')">
                <h6><span class="desc-peinture3 badge badge-primary">Teinte métalisé<br> noir profond 840€</span></h6>
            </div>
        </div>
        <h3 class="font-alpine font-italic">Jantes</h3> 
        <hr>
        <div class="jantes row">
            <div class="jante-standard col-6 " v-if="version === 'pure'">
                <img :src="imageJante[0]" v-on:click="selectStandard('img.jante', $event)" data-type="0" data-name="Jante standard" class="img-fluid jante selected">
                <h6><span class="desc-peinture3 badge badge-primary">Jante standard</span></h6>
            </div>
            <div class="jante-serac col-6" v-if="version === 'pure'">
                <img :src="imageJante[1]" v-on:click="selectSerac('img.jante', $event)" data-type="2300" data-name="Jante serac" class="img-fluid jante">
                <h6><span class="desc-peinture3 badge badge-primary">Jante serac</span></h6>
            </div>
            <div class="jante-legende col-6" v-if="version === 'legende'">
                <img :src="imageJante[2]" v-if="version === 'legende'" v-on:click="selectLegende('img.jante', $event)" data-type="3000" data-name="Jante légende" class="img-fluid jante">
                <h6><span class="desc-peinture3 badge badge-primary">Jante Legende</span></h6>
            </div>
        </div>
    </div>
</div>