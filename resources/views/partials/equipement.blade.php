<h3 class="font-alpine font-italic text-center">Equipements</h3>
<div class="transport">
<div class="col-12 col-md-6 row">
    <h5 class="font-italic font-alpine">Design</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="separation equipements row">
    <div class="extincteurs col-6 col-md-3">                         
        <img src="images/configurateur/equipements/categories/design/repose-pied-alu.jpg" data-name="Repose pied alu" data-type="34" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Repose pied alu<br>34€</span></h6>  
    </div>  
    <div class="pack-heritage col-6 col-md-3">              
        <img src="images/configurateur/equipements/categories/design/pack-heritage.jpg" data-name="Pack héritage" data-type="100" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Pack héritage<br>100€</span></h6>
    </div>
</div>
<div class="transport">
<div class="col-12 col-md-6 row">
    <h5 class="font-italic font-alpine">Media et<br> navigation</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="separation equipements row">
    <div class="alpine-metrics col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/media et navigation/alpine-metrics.jpg" data-name="Alpine metrics" data-type="59" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Alpine metrics<br>59€</span></h6>
    </div>
    <div class="audio-foc col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/media et navigation/audio-focal.jpg" data-type="0" data-name="Audio focal" data-inclus="de série" class="img-fluid z-depth-1 selected">
        <h6><span class="badge badge-primary">Audio focal</span><span class="badge badge-secondary">de série</span></h6>
    </div>
    <div class="audio-prem col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/media et navigation/audio-premium.jpg" data-name="Audio premium" data-type="79" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Audio premium<br>79€</span></h6>
    </div>
    <div class="audio-std col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/media et navigation/audio-standard.jpg" data-name="Audio standard" data-type="45" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Audio standard<br>45€</span></h6>
    </div>
</div>
<div class="transport">
<div class="col-12 col-md-6 row">
    <h5 class="font-italic font-alpine">Confort</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="equipements row">
    <div class="filet-range col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/confort/pack-rangement.jpg" data-name="Pack de rangement" data-type="504" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Pack de rangement<br>504€</span></h6>
    </div>
    <div class="regul-vitesse col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/confort/regul-limit-vitesse.jpg" data-type="0" data-name="Régulateur de vitesse" data-inclus="de série" class="img-fluid z-depth-1 selected">
        <h6><span class="badge badge-primary">Régulateur de vitesse</span><span class="badge badge-secondary">de série</span></h6>
    </div>
    <div class="retro-ext col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/confort/retro-ext-chaffant.jpg" data-name="Rétro extérieur chauffant" data-type="150" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Rétro extérieur chauffant<br>150€</span></h6>
    </div>
    <div class="retro-int col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/confort/retro-int-electrochrome.jpg" data-type="0" data-name="Rétro intérieur electrochrome" data-inclus="de série" class="img-fluid z-depth-1 selected">
        <h6><span class="badge badge-primary">Rétro intérieur electrochrome</span><span class="badge badge-secondary">de série</span></h6>
    </div>
</div>
<div class="transport">
    <div class="col-12 col-md-6 row">
        <h5 class="font-italic font-alpine">Conduite</h5>
        <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>
<div class="separation equipements row">
    <div class="aide-stationnement-ar col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/conduite/aide-stationnement-ar.jpg" data-name="Aide au stationnement arrière" data-type="148" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Aide au stationnement arrière<br>148€</span></h6>
    </div>
    <div class="aide-stationnement-av col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/conduite/aide-stationnement-av-ar.jpg" data-name="Aide au stationnement avant" data-type="148" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Aide au stationnement avant<br>448€</span></h6>
    </div>
    <div class="camera-recul col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/conduite/camera-recul.jpg" data-type="0" data-name="Caméra de recul" data-inclus="de série" class="img-fluid z-depth-1 selected">
        <h6><span class="badge badge-primary">Caméra de recul</span><span class="badge badge-secondary">de série</span></h6>
    </div>
    <div class="echap-sport col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/conduite/echappement-sport.jpg" data-name="Echappement sport" data-type="1500" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Echappement sport<br>1500€</span></h6>
    </div>
</div>
<div class="transport">
    <div class="col-12 col-md-6 row">
        <h5 class="font-italic font-alpine">Securite</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>  
<div class="separation equipements row">
    <div class="chargeur-batterie col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/securite/aide-freinage-durgence.jpg" data-type="0" data-name="Aide freinage d'urgence" data-inclus="de série" class="img-fluid z-depth-1 selected">
        <h6><span class="badge badge-primary">Aide freinage d'urgence</span><span class="badge badge-secondary">de série</span></h6>
    </div>
    <div class="airbag col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/securite/airbag.jpg"
            class="img-fluid z-depth-1 selected" data-inclus="de série" data-type="0" data-name="Airbag">
        <h6><span class="badge badge-primary">Airbag</span><span class="badge badge-secondary">de série</span></h6>
    </div>
    <div class="freinage col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/securite/freinage-haute-perf.jpg"
            class="img-fluid z-depth-1" data-type="1008" data-name="Freinage haute perf" v-on:click="selected">
        <h6><span class="badge badge-primary">Freinage haute perf<br>1008€</span></h6>
    </div>
</div>
<div class="transport">
    <div class="col-12 col-md-6 row">
        <h5 class="font-italic font-alpine">Personalisation <br>extérieur</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>  
<div class="separation equipements row">
    <div class="etrier-bleu col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/personnalisation exterieure/etrier-bleu.jpg" data-name="Etrier bleu" data-type="70" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Etrier bleu<br>70€</span></h6>
    </div>
    <div class="etrier-gris col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/personnalisation exterieure/etrier-gris.jpg"
            class="img-fluid z-depth-1" data-name="Etrier gris" data-type="70" v-on:click="selected">
        <h6><span class="badge badge-primary">Etrier-gris<br>70€</span></h6>
    </div>
    <div class=" col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/personnalisation exterieure/logo-alpine.jpg"
            class="img-fluid z-depth-1" data-type="30" data-name="Logo alpine" v-on:click="selected">
        <h6><span class="badge badge-primary">Logo alpine<br>30€</span></h6>
    </div>
</div>
<div class="transport">
    <div class="col-12 col-md-6 row">
        <h5 class="font-italic font-alpine">Personalisation <br>intérieur</h5>
    <div class="col-md-4">
        <div class="slash"></div>
    </div>
</div>
</div>  
<div class="separation accessoires row">
    <div class="logo-volant col-6 col-md-3">
        <img src="images/configurateur/equipements/categories/personnalisation interieure/logo-volant.jpg" data-name="Logo volant" data-type="20" class="img-fluid z-depth-1" v-on:click="selected">
        <h6><span class="badge badge-primary">Logo volant<br>20€</span></h6>
    </div>
    <div class="pack-carbone col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/personnalisation interieure/pack-carbone.jpg"
            class="img-fluid z-depth-1" data-name="Pack carbone" data-type="110" v-on:click="selected">
        <h6><span class="badge badge-primary">Pack carbone<br>110€</span></h6>
    </div>
    <div class="pedale-alu col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/personnalisation interieure/pedal-alu.jpg"
            class="img-fluid z-depth-1" data-type="106" data-name="Pedale en alu" v-on:click="selected">
        <h6><span class="badge badge-primary">Pedale aluminium<br>106€</span></h6>
    </div>
    <div class="siege-chauffant col-6 col-md-3">
            <img src="images/configurateur/equipements/categories/personnalisation interieure/siege-chauffant.jpg"
            class="img-fluid z-depth-1" data-name="Siège chauffant" data-type="180" v-on:click="selected">
        <h6><span class="badge badge-primary">Siège chauffant<br>180€</span></h6>
    </div>
</div>