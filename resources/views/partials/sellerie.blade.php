<h3 class="font-alpine font-italic text-center">Sellerie</h3>
<div class="sellerie row">
    <div v-if="version === 'legende'" class="  cuir-brun col-6">
        <img src="images/configurateur/interieurs/selection/cuir-brun.jpg" data-type="0" data-name="Siège confort en cuir brun" class="siege img-fluid z-depth-1" v-on:click="selectUnique('img.siege', $event)">
        <h6><span class=" badge badge-primary">Siège confort en<br>cuir brun</span></h6>
    </div>
    <div v-if="version === 'pure'" class="  cuir-noir col-6">
        <img src="images/configurateur/interieurs/selection/cuir-noir.jpg" data-type="0" data-name="Siège confort en cuir noir" class="siege img-fluid z-depth-1" v-on:click="selectUnique('img.siege', $event)">
        <h6><span class=" badge badge-primary">Siège confort en<br>cuir noir</span></h6>
    </div>
    <div v-if="version === 'pure'" class="  cuir-brun-dina col-6">
        <img src="images/configurateur/interieurs/selection/cuir-noir_dinamica.jpg" data-type="0" data-name="Siège en cuir noir et dinamica"  class="siege img-fluid z-depth-1 selected" v-on:click="selectUnique('img.siege', $event)">
        <h6><span class=" badge badge-primary">Siège en cuir noir<br>et dinamica</span></h6>
    </div>
    <div v-if="version === 'legende'" class="  cuir-brun-perfore col-6">
        <img src="images/configurateur/interieurs/selection/cuir-noir_perfore.jpg" data-type="0" data-name="Siège confort en cuir noir" class="siege img-fluid z-depth-1 " v-on:click="selectUnique('img.siege', $event)">
        <h6><span class="badge badge-primary">Siège confort en<br>cuir noir perforé</span></h6>
    </div>
</div>